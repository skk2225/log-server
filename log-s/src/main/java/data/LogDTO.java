package data;

public class LogDTO {
    
	private long id;
	private String timestamp;
	private int status;
	private String error;
	private String ipAddress;
	private String method;
	private String path;

    public LogDTO() {

    }

    public LogDTO(final Log log) {
        setId(log.getId());
        setTimestamp(log.getTimestamp());
        setStatus(log.getStatus());
        setError(log.getError());
        setIpAddress(log.getIpAddress());
        setMethod(log.getMethod());
        setPath(log.getPath());
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "Log [timestamp = " + timestamp + ", status = " + status + ", error = " + error + ", ipAddress = "
				+ ipAddress + ", method = " + method + ", path = " + path + "]";
	}
}

