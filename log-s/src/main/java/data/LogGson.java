package data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import data.serializers.LogDeserializer;
import data.serializers.LogSerializer;

public class LogGson {
    private final Gson gson;

    public LogGson() {
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Log.class, new LogSerializer());
        gsonBuilder.registerTypeAdapter(Log.class, new LogDeserializer());
        gson = gsonBuilder.create();
    }

    public Gson getGson() {
        return gson;
    }
}

