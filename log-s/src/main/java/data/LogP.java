package data;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity

@NamedQueries({
@NamedQuery(name = "LogP.findLog", 
query = "SELECT p.id " +
        "FROM LogP p " +
        "WHERE p.id=:id"),
@NamedQuery(name = "LogP.getAllLogs", query = "SELECT p FROM LogP p"),
@NamedQuery(name = "LogP.getAllLogsByStatus",
query = "SELECT p FROM LogP p WHERE p.status=:status")})
public class LogP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Basic
	private String timestamp;
	
	@Basic
	private int status;
	
	@Basic
	private String error;
	
	@Basic
	private String ipAddress;
	
	@Basic
	private String method;
	
	@Basic
	private String path;
	
	public LogP(String timestamp, int status, String error, String ipAddress, String method, String path) {
		super();
		this.timestamp = timestamp;
		this.status = status;
		this.error = error;
		this.ipAddress = ipAddress;
		this.method = method;
		this.path = path;
	}
	public LogP() {
		super();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	@Override
    public String toString() {
        return "Log [timestamp = " + timestamp + ", status = " + status + ", error = " + error
                + ", ipAddress = " + ipAddress + ", method = " + method + ", path = " + path + "]";
    }
	
}

