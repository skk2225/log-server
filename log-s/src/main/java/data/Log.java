package data;

import java.util.Objects;

public class Log {

	private long id;
	private String timestamp;
	private int status;
	private String error;
	private String ipAddress;
	private String method;
	private String path;

	public Log() {
		this("", 0, "", "", "", "");
	}

	public Log(String timestamp, int status, String error, String ipAddress, String method, String path) {
		// TODO Auto-generated constructor stub
		this.timestamp = timestamp;
		this.status = status;
		this.error = error;
		this.ipAddress = ipAddress;
		this.method = method;
		this.path = path;
	}

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Log other = (Log) obj;
		if (id != id) {
			return false;
		}
		if (timestamp.equals(other.timestamp)) {
			return false;
		}
		if (status != other.status) {
			return false;
		}
		if (!error.equals(other.error)) {
			return false;
		}
		if (!ipAddress.equals(other.ipAddress)) {
			return false;
		}
		if (!method.equals(other.method)) {
			return false;
		}
		if (!path.equals(other.path)) {
			return false;
		}
		return true;
	}

	public void updateLog(final Log newLog) {
		setTimestamp(newLog.getTimestamp());
        setStatus(newLog.getStatus());
        setError(newLog.getError());
        setIpAddress(newLog.getIpAddress());
        setMethod(newLog.getMethod());
        setPath(newLog.getPath());
	}

	@Override
	public String toString() {
		return "Log [timestamp = " + timestamp + ", status = " + status + ", error = " + error + ", ipAddress = "
				+ ipAddress + ", method = " + method + ", path = " + path + "]";
	}
}
