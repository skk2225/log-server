package data;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class DataProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataProcessor.class);
    private static final String LOGS_JSON_ELEMENT = "logs";

    private final Gson gson = (new LogGson()).getGson();
    private final Set<Log> logs = new HashSet<Log>();

    public void updateData(final Log newLog) throws FileNotFoundException {
        final Type logSetType = new TypeToken<HashSet<Log>>() {
        }.getType();
        Log log;
        try {
            log = getLogById(newLog.getId());
        } catch (final NoSuchElementException e) {
            log = new Log();
            log.setId(newLog.getId());
            logs.add(log);
            LOGGER.info("Created new log with id: {}", newLog.getId());
        }

        log.updateLog(newLog);
        final JsonObject jsonObject = new JsonObject();
        jsonObject.add(LOGS_JSON_ELEMENT, gson.toJsonTree(logs, logSetType));

    }

    public Log getLogById(final long id) {
        return logs.stream().filter((log) -> log.getId() == id)
                .findAny().orElseThrow(NoSuchElementException::new);
    }

    public Set<LogDTO> getLogsAsDTO() {
        return logs.stream().map(LogDTO::new).collect(Collectors.toCollection(HashSet::new));
    }

    public Map<String, Set<LogDTO>> getLogsAsObject() {
        final Map<String, Set<LogDTO>> logs = new HashMap<>();
        logs.put(LOGS_JSON_ELEMENT, getLogsAsDTO());
        return logs;
    }
}

