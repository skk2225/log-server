package data;

import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class LogDAO {
	
	public LogDAO(){
		
	}
	
	public String getLogs(){
		EntityManager entityManager = InitEntityManager.getEntityManager();
		String allLogs = "";
		try{
		Query query = entityManager.createNamedQuery("LogP.getAllLogs");
		List results = query.getResultList();
		for(Object p:results){
			allLogs+=p + "\n";
		}
		}finally{
			entityManager.close();
		}
		return allLogs;
	}
	
	public String getLogsForJson(){
		 Gson gson = new Gson();
		EntityManager entityManager = InitEntityManager.getEntityManager();
		List results;
		try{
		Query query = entityManager.createNamedQuery("LogP.getAllLogs");
		results = query.getResultList();
		}finally{
			entityManager.close();
		}
		String jsonLogs = gson.toJson(results);
		return jsonLogs;
	}
	
	public String getLogsByStatus(int status){
		EntityManager entityManager = InitEntityManager.getEntityManager();
		String allLogs = "";
		try{
		Query query = entityManager.createNamedQuery("LogP.getAllLogsByStatus");
		query.setParameter("status", status);
		List results = query.getResultList();
		for(Object p:results){
			allLogs+=p + "\n";
		}
		}finally{
			entityManager.close();
		}
		return allLogs;
	}
	
	public String getLogsByStatusForJson(int status){
		EntityManager entityManager = InitEntityManager.getEntityManager();
		String allLogs = "";
		List results;
		try{
		Query query = entityManager.createNamedQuery("LogP.getAllLogsByStatus");
		query.setParameter("status", status);
		results = query.getResultList();
		for(Object p:results){
			allLogs+=p + "\n";
		}
		}finally{
			entityManager.close();
		}
		Gson gson = new Gson();
		String jsonLogs = gson.toJson(results);
		return jsonLogs;
	}
	
	public String getLogsHTML(){
		String out = "";
	    out = out + "<html>\n";
	    out = out + "<head><title>All Logs</title></head>\n";
	    out = out + "<body>\n";
	    out = out + "<center><h1>All Logs</h1>\n";
	    out = out + "<p>";
		EntityManager entityManager = InitEntityManager.getEntityManager();
		String allLogs = "";
		try{
		Query query = entityManager.createNamedQuery("LogP.getAllLogs");
		List results = query.getResultList();
		for(Object p:results){
			allLogs+=p + "</ br>";
		}
		}finally{
			entityManager.close();
		}
		out = out + allLogs;
		out = out + "<p>\n";
		out = out + "</center>\n";
	    out = out + "</body>\n";
	    out = out + "</html>";
		return out;
	}
	
	public String getLogById(long id){
		EntityManager entityManager = InitEntityManager.getEntityManager();
		LogP pLog;
		try{
		pLog = entityManager.find(LogP.class,id);
		}finally{
			entityManager.close();
		}
        return "Id: " + pLog.getId() + ", Timestamp: " + pLog.getTimestamp() + 
        		", Status: " + pLog.getStatus() + ", Error: " + pLog.getError();
    }
	
	public String getLogByIdForJson(long id){
		EntityManager entityManager = InitEntityManager.getEntityManager();
		LogP pLog;
		try{
		pLog = entityManager.find(LogP.class,id);
		}finally{
			entityManager.close();
		}
		Gson gson = new Gson();
		String jsonLogs = gson.toJson(pLog);
		return jsonLogs;
    }
	
	public void deleteLogById(long id){
		EntityManager entityManager = InitEntityManager.getEntityManager();
		try{
		LogP pLog = entityManager.find(LogP.class,id);
		
		entityManager.getTransaction().begin();
		entityManager.remove(pLog);
		entityManager.getTransaction().commit();
		} finally{
			entityManager.close();
		}
	}
	
	public void deleteAllLogs(){
		EntityManager entityManager = InitEntityManager.getEntityManager();
		try{
			entityManager.getTransaction().begin();
			int deletedCount = entityManager.createQuery("DELETE FROM LogP").executeUpdate();
			entityManager.getTransaction().commit();
		}finally{
			entityManager.close();
		}
	}
    
    public void saveToDB (Log log)
    {
    	EntityManager entityManager = InitEntityManager.getEntityManager();
    	try{
		LogP logInst = new LogP(log.getTimestamp(), log.getStatus(), log.getError(),
					log.getIpAddress(), log.getMethod(), log.getPath());
			entityManager.getTransaction().begin();
			entityManager.persist(logInst);
			entityManager.getTransaction().commit();
    	}finally{
    		entityManager.close();
    	}
    }
    
    public String saveToDBJson (Log log)
    {
    	EntityManager entityManager = InitEntityManager.getEntityManager();
    	try{
		LogP logInst = new LogP(log.getTimestamp(), log.getStatus(), log.getError(),
					log.getIpAddress(), log.getMethod(), log.getPath());
			entityManager.getTransaction().begin();
			entityManager.persist(logInst);
			entityManager.getTransaction().commit();
    	}finally{
    		entityManager.close();
    	}
    	Gson gson = new Gson();
		String jsonLogs = gson.toJson(log);
		return jsonLogs;
    }
}
