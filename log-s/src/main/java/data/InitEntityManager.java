package data;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class InitEntityManager {

    private static EntityManagerFactory emf;
    
    private static void createEmf() {
        try {
            emf = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        } catch (Exception e) {
				e.printStackTrace();
        }
    }

	public static EntityManager getEntityManager() {
		if(emf==null){
		createEmf();
		}
		return emf.createEntityManager();
	}
	
}

