package data.serializers;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import data.Log;

public class LogDeserializer implements JsonDeserializer<Log> {

    @Override
    public Log deserialize(final JsonElement logAsJson, final Type type, final JsonDeserializationContext context)
            throws JsonParseException {
        final JsonObject jsonObject = logAsJson.getAsJsonObject();

        final Log log = new Log(jsonObject.get("timestamp").getAsString(),
                jsonObject.get("status").getAsInt(),
                jsonObject.get("error").getAsString(),
                jsonObject.get("ipAddress").getAsString(),
                jsonObject.get("method").getAsString(),
                jsonObject.get("path").getAsString());

        return log;
    }

}

