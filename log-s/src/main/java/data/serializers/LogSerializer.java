package data.serializers;

import java.lang.reflect.Type;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import data.Log;

public class LogSerializer implements JsonSerializer<Log> {
    @Override
    public JsonElement serialize(final Log log, final Type type,
            final JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("timestamp", log.getTimestamp());
        jsonObject.addProperty("status", log.getStatus());
        jsonObject.addProperty("error", log.getError());
        jsonObject.addProperty("ipAddress", log.getIpAddress());
        jsonObject.addProperty("message", log.getMethod());
        jsonObject.addProperty("path", log.getPath());

        return jsonObject;
    }
}
