package rest;
import org.glassfish.jersey.media.sse.SseFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class RESTActivator extends ResourceConfig {

    public RESTActivator() {
        registerClasses(RESTService.class);
        register(RolesAllowedDynamicFeature.class);
        register(SseFeature.class);
    }
}
