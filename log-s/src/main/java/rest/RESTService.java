package rest;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseBroadcaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import data.DataProcessor;
import data.Log;
import data.LogDAO;
import data.LogGson;
import data.LogP;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("")
@PermitAll
public class RESTService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RESTService.class);

	private static final SseBroadcaster broadcaster = new SseBroadcaster();
	
	@Context
	protected HttpServletRequest httpRequest;

	private final DataProcessor dataProcessor = new DataProcessor();
	private final LogDAO logDAO = new LogDAO();
	private final Gson gson = (new LogGson()).getGson();
	
	/**
	 * Methods handling HTTP GET/POST/DELETE requests. The returned object will be sent to
	 * the client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	
//	@POST
//	@Path("logs")
//	@Consumes(MediaType.APPLICATION_JSON)
//	public Response uploadInfoAboutLog(final String logAsString) {
//		final Log log;
//		try {
//			log = gson.fromJson(logAsString, Log.class);
//		} catch (final JsonSyntaxException e) {
//			LOGGER.error("Cannot parse JSON: {}", logAsString, e);
//			return Response.status(Response.Status.BAD_REQUEST).build();
//		}
//
//		logDAO.saveToDB(log);
//
//		broadcaster.broadcast(createItemForBroadcast());
//		LOGGER.info("Broadcast updated data.");
//
//		return Response.ok("OK").build();
//	}
	
	@POST
	@Path("logs")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response uploadInfoAboutLog(@FormParam("timestamp") String timestamp,
			@FormParam("status") int status,
			@FormParam("error") String error,
			@FormParam("ipAddres") String ipAddress,
			@FormParam("method") String method,
			@FormParam("path") String path) {
		Log log = new Log();
		try {
			log.setTimestamp(timestamp);
			log.setError(error);
			log.setIpAddress(ipAddress);
			log.setMethod(method);
			log.setPath(path);
			log.setStatus(status);
		} catch (final JsonSyntaxException e) {
			LOGGER.error("Cannot parse JSON: {}", "", e);
		}

		logDAO.saveToDB(log);

		broadcaster.broadcast(createItemForBroadcast());
		LOGGER.info("Broadcast updated data.");

		return Response.ok("OK").build();
	}
	
//	@GET
//	@Path("logs/{id}")
//	@Produces(MediaType.TEXT_PLAIN)
//	public Response getLogs(@PathParam("id") final long logId){
//		return Response.ok(logDAO.getLogById(logId)).build();
//	}
	
//	@GET
//	@Path("logs")
//	@Produces(MediaType.TEXT_PLAIN)
//	public Response getLogs(){
//		return Response.ok(logDAO.getLogs()).build();
//	}
	
	@GET
	@Path("logs/byId")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLogs(@QueryParam("id") final long logId){
		return Response.ok(logDAO.getLogByIdForJson(logId)).build();
	}
	
	@GET
	@Path("logs")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLogs(){
		return logDAO.getLogsForJson();
	}
	
//	@GET
//	@Path("logs")
//	@Produces(MediaType.TEXT_HTML)
//	public Response getLogs(){
//		return Response.ok(logDAO.getLogsHTML()).build();
//	}
	
//	@GET
//	@Path("logs/status/{status}")
//	@Produces(MediaType.TEXT_PLAIN)
//	public Response getLogsByStatus(@PathParam("status") final int logStatus){
//		return Response.ok(logDAO.getLogsByStatus(logStatus)).build();
//	}
	
	@GET
	@Path("logs/status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLogsByStatus(@QueryParam("status") final int logStatus){
		return Response.ok(logDAO.getLogsByStatusForJson(logStatus)).build();
	}
	
//	@DELETE
//	@Path("logs/{id}")
//	public Response deleteLogById(@PathParam("id") final long logId) {
//		logDAO.deleteLogById(logId);
//		return Response.ok("OK").build();
//	}
	
	@DELETE
	@Path("logs/byId")
	public Response deleteLogById(@QueryParam("id") final long logId) {
		logDAO.deleteLogById(logId);
		return Response.ok("OK").build();
	}
	
	@DELETE
	@Path("logs")
	public Response deleteAllLogs() {
		logDAO.deleteAllLogs();
		return Response.ok("OK").build();
	}
	
//	@DELETE
//	@Path("logs")
//	public Response deleteAllLogs() {
//		logDAO.deleteAllLogs();
//		return Response.ok("OK").build();
//	}
	
	private String getLogsAsJsonString() {
		return gson.toJson(dataProcessor.getLogsAsObject());
	}
	
	private OutboundEvent createItemForBroadcast() {
		return new OutboundEvent.Builder().id("logData")
				.data(String.class, getLogsAsJsonString()).mediaType(MediaType.APPLICATION_JSON_TYPE).build();
	}
}
